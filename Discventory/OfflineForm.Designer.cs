﻿namespace Discventory
{
    partial class OfflineForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.lstOflineItems = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbBarcode = new System.Windows.Forms.TextBox();
            this.cbDupeAllow = new System.Windows.Forms.CheckBox();
            this.reenableScanTimer = new System.Windows.Forms.Timer();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.menuItem2);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Add";
            // 
            // lstOflineItems
            // 
            this.lstOflineItems.Location = new System.Drawing.Point(4, 36);
            this.lstOflineItems.Name = "lstOflineItems";
            this.lstOflineItems.Size = new System.Drawing.Size(233, 198);
            this.lstOflineItems.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 16);
            this.label1.Text = "Bar/Cat";
            // 
            // tbBarcode
            // 
            this.tbBarcode.Location = new System.Drawing.Point(62, 4);
            this.tbBarcode.Name = "tbBarcode";
            this.tbBarcode.Size = new System.Drawing.Size(175, 21);
            this.tbBarcode.TabIndex = 2;
            this.tbBarcode.GotFocus += new System.EventHandler(this.tbBarcode_GotFocus);
            this.tbBarcode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbBarcode_KeyUp);
            // 
            // cbDupeAllow
            // 
            this.cbDupeAllow.Location = new System.Drawing.Point(4, 247);
            this.cbDupeAllow.Name = "cbDupeAllow";
            this.cbDupeAllow.Size = new System.Drawing.Size(121, 20);
            this.cbDupeAllow.TabIndex = 4;
            this.cbDupeAllow.Text = "Allow duplicates";
            // 
            // reenableScanTimer
            // 
            this.reenableScanTimer.Interval = 500;
            this.reenableScanTimer.Tick += new System.EventHandler(this.reenableScanTimer_Tick);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Delete";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // OfflineForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.cbDupeAllow);
            this.Controls.Add(this.tbBarcode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstOflineItems);
            this.Menu = this.mainMenu1;
            this.Name = "OfflineForm";
            this.Text = "Offline Mode";
            this.Load += new System.EventHandler(this.OfflineForm_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OfflineForm_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lstOflineItems;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBarcode;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.CheckBox cbDupeAllow;
        private System.Windows.Forms.Timer reenableScanTimer;
        private System.Windows.Forms.MenuItem menuItem2;
    }
}