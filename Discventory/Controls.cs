﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Discventory
{
    
    public class ThinBar : UserControl
    {
        private double _position = 0;
        public int Minimum = 0;
        public int Maximum = 100;

        public int position
        {
            get
            {
                return Convert.ToInt32(Maximum * _position) + Minimum;
            }
            set
            {
                _position = Convert.ToDouble(value) / Convert.ToDouble(Maximum - Minimum);
                this.Refresh();
            }
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(BackColor), 0, 0, Width, Height);
            e.Graphics.FillRectangle(new SolidBrush(ForeColor), 0, 0, Convert.ToInt32(_position * Width), Height);
            
        }
        protected override void OnPaint(PaintEventArgs e)
        {
           
        }
    }
    public class IDTMProgressBar : UserControl
    {
        private Timer aniTimer = new Timer();
        private double position = -0.4;
        private int padding = 2;
       
        public IDTMProgressBar()
        {
            aniTimer.Interval = 100;
            aniTimer.Enabled = false;
            aniTimer.Tick += this._tick;
        }
        public void Start()
        {
            aniTimer.Enabled = true;
        }
        private void _tick(object sender, EventArgs e)
        {
            position += 0.1;
            if (position >= 1) position = -0.4;
            this.Invalidate();
        }
        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.FillRectangle((new SolidBrush(Color.DarkBlue)), Convert.ToInt32(Width * position) + padding, padding, Width / 3, Height - 2 * padding);
        }
        Color cBG = new Button().BackColor;
     
        protected override void OnPaintBackground(PaintEventArgs e)
        {
           
            e.Graphics.FillRectangle(new SolidBrush(cBG), 1, 1, Width-2, Height-2);
            e.Graphics.DrawRectangle(new Pen(Color.Black), 0, 0, Width-1, Height-1);
        }
    }
}
