﻿namespace Discventory
{
    partial class Barsearch
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.cbFolder = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.autoAdd = new System.Windows.Forms.CheckBox();
            this.lblReleaseNumber = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbQuery = new System.Windows.Forms.TextBox();
            this.lblAlbum = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.lstResult = new System.Windows.Forms.ListBox();
            this.lblEtc = new System.Windows.Forms.Label();
            this.firstMatch = new System.Windows.Forms.CheckBox();
            this.reenableScanTimer = new System.Windows.Forms.Timer();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            this.mainMenu1.MenuItems.Add(this.menuItem2);
            // 
            // menuItem1
            // 
            this.menuItem1.Text = "Search";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Add Selected";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // cbFolder
            // 
            this.cbFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFolder.Location = new System.Drawing.Point(67, 3);
            this.cbFolder.Name = "cbFolder";
            this.cbFolder.Size = new System.Drawing.Size(170, 22);
            this.cbFolder.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.Text = "Add to:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(67, 31);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(170, 21);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.GotFocus += new System.EventHandler(this.textBox1_GotFocus);
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(4, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 20);
            this.label2.Text = "Barcode:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(4, 85);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(105, 106);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // autoAdd
            // 
            this.autoAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.autoAdd.Location = new System.Drawing.Point(4, 245);
            this.autoAdd.Name = "autoAdd";
            this.autoAdd.Size = new System.Drawing.Size(82, 20);
            this.autoAdd.TabIndex = 5;
            this.autoAdd.Text = "Auto add";
            // 
            // lblReleaseNumber
            // 
            this.lblReleaseNumber.Location = new System.Drawing.Point(115, 84);
            this.lblReleaseNumber.Name = "lblReleaseNumber";
            this.lblReleaseNumber.Size = new System.Drawing.Size(122, 20);
            this.lblReleaseNumber.Text = "CD06438";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(4, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 20);
            this.label3.Text = "Query:";
            // 
            // tbQuery
            // 
            this.tbQuery.Location = new System.Drawing.Point(67, 58);
            this.tbQuery.Name = "tbQuery";
            this.tbQuery.Size = new System.Drawing.Size(170, 21);
            this.tbQuery.TabIndex = 3;
            // 
            // lblAlbum
            // 
            this.lblAlbum.Location = new System.Drawing.Point(115, 104);
            this.lblAlbum.Name = "lblAlbum";
            this.lblAlbum.Size = new System.Drawing.Size(119, 47);
            this.lblAlbum.Text = "Armin van Buuren - A State Of Trance 2013";
            // 
            // lblYear
            // 
            this.lblYear.Location = new System.Drawing.Point(115, 153);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(122, 20);
            this.lblYear.Text = "2013";
            // 
            // lstResult
            // 
            this.lstResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstResult.Location = new System.Drawing.Point(4, 197);
            this.lstResult.Name = "lstResult";
            this.lstResult.Size = new System.Drawing.Size(230, 44);
            this.lstResult.TabIndex = 4;
            this.lstResult.Visible = false;
            this.lstResult.SelectedIndexChanged += new System.EventHandler(this.lstResult_SelectedIndexChanged);
            // 
            // lblEtc
            // 
            this.lblEtc.Location = new System.Drawing.Point(115, 173);
            this.lblEtc.Name = "lblEtc";
            this.lblEtc.Size = new System.Drawing.Size(119, 17);
            this.lblEtc.Text = "Russia CD Compilation Mixed";
            // 
            // firstMatch
            // 
            this.firstMatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.firstMatch.Location = new System.Drawing.Point(141, 245);
            this.firstMatch.Name = "firstMatch";
            this.firstMatch.Size = new System.Drawing.Size(93, 20);
            this.firstMatch.TabIndex = 6;
            this.firstMatch.Text = "1st only";
            // 
            // reenableScanTimer
            // 
            this.reenableScanTimer.Interval = 500;
            this.reenableScanTimer.Tick += new System.EventHandler(this.reenableScanTimer_Tick);
            // 
            // Barsearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.firstMatch);
            this.Controls.Add(this.lblEtc);
            this.Controls.Add(this.lstResult);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.lblAlbum);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbQuery);
            this.Controls.Add(this.lblReleaseNumber);
            this.Controls.Add(this.autoAdd);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbFolder);
            this.Menu = this.mainMenu1;
            this.MinimizeBox = false;
            this.Name = "Barsearch";
            this.Text = "Barcode Search";
            this.Load += new System.EventHandler(this.Barsearch_Load);
            this.Activated += new System.EventHandler(this.Barsearch_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Barsearch_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox autoAdd;
        private System.Windows.Forms.Label lblReleaseNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbQuery;
        private System.Windows.Forms.Label lblAlbum;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.ListBox lstResult;
        private System.Windows.Forms.Label lblEtc;
        private System.Windows.Forms.CheckBox firstMatch;
        private System.Windows.Forms.Timer reenableScanTimer;
    }
}