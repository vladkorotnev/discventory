﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
#if DSIC_SCANNER
using DSIC.Barcode;
#endif

namespace Discventory
{
    public partial class OfflineForm : Form
    {
        Toast myToast;

        public OfflineForm()
        {
            InitializeComponent();
        }

        void DoAdd()
        {
            if (cbDupeAllow.Checked || !lstOflineItems.Items.Contains(tbBarcode.Text))
            {
                lstOflineItems.Items.Add(tbBarcode.Text);
            }
            else
            {
                myToast.Text = "Duplicate!";
                myToast.Show(500);
            }
            tbBarcode.Text = "";
        }

#if DSIC_SCANNER
        private void BarcodeEvent(Wrapper.DS_BARCODE e)
        {
            tbBarcode.Text = e.strBarcode;
            DoAdd();
            if (hasScanner)
            {
                reenableScanTimer.Enabled = true;
            }
        }
#endif

        bool hasScanner = false;

        private void OfflineForm_Load(object sender, EventArgs e)
        {
            myToast = new Toast(this, Color.White, "Duplicate!");
            if (File.Exists("offline.dsk"))
            {
               StreamReader sr =  File.OpenText("offline.dsk");
               while (!sr.EndOfStream)
               {
                   lstOflineItems.Items.Add(sr.ReadLine());
               }
               sr.Close();
            }

#if DSIC_SCANNER

            Wrapper.PDA_MODEL model = Wrapper.PDA_MODEL.UNKNOWN_MODEL;
            Wrapper.DSBarcodeGetPDAModel(ref model);
            if (model != Wrapper.PDA_MODEL.UNKNOWN_MODEL)
            {
                hasScanner = true;
                Wrapper.DSBarcodeOpen();

                Wrapper.DSBarcodeSetTimeOut(5000);

                Wrapper.DS3_DS5_SCANKEY keyOpt5 = new Wrapper.DS3_DS5_SCANKEY();
                {
                    keyOpt5.bLeft = true;
                    keyOpt5.bRight = true;
                    keyOpt5.bMiddle = true;
                    keyOpt5.bTrigger = true;
                }
                Wrapper.DSBarcodeSetDS3ScanKey(keyOpt5);

                Wrapper.DS7_SCANKEY keyOpt7 = new Wrapper.DS7_SCANKEY();
                {
                    keyOpt7.bLeftDown = true;
                    keyOpt7.bRightDown = true;
                }

                Wrapper.pCallBack barcodeCallback = new Wrapper.pCallBack(this.BarcodeEvent);
                Wrapper.DSBarcodeSetCallback(barcodeCallback);
                Wrapper.DSBarcodeSetSymbologyAllEnable();

                myToast.Text = "DSIC Scanner detected!";
                myToast.Show(2000);
            }
            else
            {

            }
#endif
        }

        private void tbBarcode_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // Enter
                DoAdd();
            }
        }

        private void OfflineForm_Closing(object sender, CancelEventArgs e)
        {
#if DSIC_SCANNER
            if (hasScanner)
            {
                reenableScanTimer.Enabled = false;
                Wrapper.DSBarcodeScanStop();
                Wrapper.DSBarcodeClose();
            }
#endif
            if (File.Exists("offline.dsk"))
            {
                File.Delete("offline.dsk");
            }
            if (lstOflineItems.Items.Count > 0)
            {
                StreamWriter sw = File.CreateText("offline.dsk");
                foreach (String item in lstOflineItems.Items)
                {
                    sw.WriteLine(item);
                }
                sw.Flush();
                sw.Close();
            }
        }

        private void reenableScanTimer_Tick(object sender, EventArgs e)
        {
            reenableScanTimer.Enabled = false;
#if DSIC_SCANNER
            Wrapper.DSBarcodeSetTimeOut(5000);
            Wrapper.DSBarcodeScanStart();
#endif
        }

        private void tbBarcode_GotFocus(object sender, EventArgs e)
        {
            if (hasScanner)
            {
                reenableScanTimer.Enabled = true;
            }
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            if(lstOflineItems.SelectedIndex >= 0 && lstOflineItems.SelectedIndex < lstOflineItems.Items.Count)
              lstOflineItems.Items.RemoveAt(lstOflineItems.SelectedIndex);
        }
    }
}