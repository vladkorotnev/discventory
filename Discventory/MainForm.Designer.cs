﻿namespace Discventory
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.table = new System.Windows.Forms.DataGrid();
            this.cbFolder = new System.Windows.Forms.ComboBox();
            this.toolBar1 = new System.Windows.Forms.ToolBar();
            this.btnSearch = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
            this.btnFirst = new System.Windows.Forms.ToolBarButton();
            this.btnPrev = new System.Windows.Forms.ToolBarButton();
            this.btnNext = new System.Windows.Forms.ToolBarButton();
            this.btnLast = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
            this.btnReload = new System.Windows.Forms.ToolBarButton();
            this.btnLogout = new System.Windows.Forms.ToolBarButton();
            this.toolbarImageList = new System.Windows.Forms.ImageList();
            this.btnOfflineSync = new System.Windows.Forms.ToolBarButton();
            this.SuspendLayout();
            // 
            // table
            // 
            this.table.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.table.Location = new System.Drawing.Point(0, 22);
            this.table.Name = "table";
            this.table.RowHeadersVisible = false;
            this.table.Size = new System.Drawing.Size(240, 246);
            this.table.TabIndex = 1;
            // 
            // cbFolder
            // 
            this.cbFolder.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbFolder.Location = new System.Drawing.Point(0, 0);
            this.cbFolder.Name = "cbFolder";
            this.cbFolder.Size = new System.Drawing.Size(240, 22);
            this.cbFolder.TabIndex = 2;
            this.cbFolder.SelectedIndexChanged += new System.EventHandler(this.cbFolder_SelectedIndexChanged);
            // 
            // toolBar1
            // 
            this.toolBar1.Buttons.Add(this.btnSearch);
            this.toolBar1.Buttons.Add(this.btnOfflineSync);
            this.toolBar1.Buttons.Add(this.toolBarButton1);
            this.toolBar1.Buttons.Add(this.btnFirst);
            this.toolBar1.Buttons.Add(this.btnPrev);
            this.toolBar1.Buttons.Add(this.btnNext);
            this.toolBar1.Buttons.Add(this.btnLast);
            this.toolBar1.Buttons.Add(this.toolBarButton2);
            this.toolBar1.Buttons.Add(this.btnReload);
            this.toolBar1.Buttons.Add(this.btnLogout);
            this.toolBar1.ImageList = this.toolbarImageList;
            this.toolBar1.Name = "toolBar1";
            this.toolBar1.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
            // 
            // btnSearch
            // 
            this.btnSearch.ImageIndex = 0;
            this.btnSearch.ToolTipText = "Barcode Search";
            // 
            // toolBarButton1
            // 
            this.toolBarButton1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // btnFirst
            // 
            this.btnFirst.Enabled = false;
            this.btnFirst.ImageIndex = 2;
            this.btnFirst.ToolTipText = "First page";
            // 
            // btnPrev
            // 
            this.btnPrev.Enabled = false;
            this.btnPrev.ImageIndex = 3;
            this.btnPrev.ToolTipText = "Previous Page";
            // 
            // btnNext
            // 
            this.btnNext.Enabled = false;
            this.btnNext.ImageIndex = 4;
            this.btnNext.ToolTipText = "Next Page";
            // 
            // btnLast
            // 
            this.btnLast.Enabled = false;
            this.btnLast.ImageIndex = 5;
            this.btnLast.ToolTipText = "Last Page";
            // 
            // toolBarButton2
            // 
            this.toolBarButton2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // btnReload
            // 
            this.btnReload.ImageIndex = 6;
            // 
            // btnLogout
            // 
            this.btnLogout.ImageIndex = 1;
            this.btnLogout.ToolTipText = "Exit/Logout";
            // 
            // toolbarImageList
            // 
            this.toolbarImageList.ImageSize = new System.Drawing.Size(32, 32);
            this.toolbarImageList.Images.Clear();
            this.toolbarImageList.Images.Add(((System.Drawing.Icon)(resources.GetObject("resource"))));
            this.toolbarImageList.Images.Add(((System.Drawing.Icon)(resources.GetObject("resource1"))));
            this.toolbarImageList.Images.Add(((System.Drawing.Icon)(resources.GetObject("resource2"))));
            this.toolbarImageList.Images.Add(((System.Drawing.Icon)(resources.GetObject("resource3"))));
            this.toolbarImageList.Images.Add(((System.Drawing.Icon)(resources.GetObject("resource4"))));
            this.toolbarImageList.Images.Add(((System.Drawing.Icon)(resources.GetObject("resource5"))));
            this.toolbarImageList.Images.Add(((System.Drawing.Icon)(resources.GetObject("resource6"))));
            this.toolbarImageList.Images.Add(((System.Drawing.Icon)(resources.GetObject("resource7"))));
            // 
            // btnOfflineSync
            // 
            this.btnOfflineSync.Enabled = false;
            this.btnOfflineSync.ImageIndex = 7;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.toolBar1);
            this.Controls.Add(this.table);
            this.Controls.Add(this.cbFolder);
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Text = "Discventory";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Activated += new System.EventHandler(this.MainForm_Activated);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MainForm_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid table;
        private System.Windows.Forms.ComboBox cbFolder;
        private System.Windows.Forms.ToolBar toolBar1;
        private System.Windows.Forms.ToolBarButton btnSearch;
        private System.Windows.Forms.ImageList toolbarImageList;
        private System.Windows.Forms.ToolBarButton btnLogout;
        private System.Windows.Forms.ToolBarButton toolBarButton1;
        private System.Windows.Forms.ToolBarButton btnFirst;
        private System.Windows.Forms.ToolBarButton btnPrev;
        private System.Windows.Forms.ToolBarButton btnNext;
        private System.Windows.Forms.ToolBarButton btnLast;
        private System.Windows.Forms.ToolBarButton toolBarButton2;
        private System.Windows.Forms.ToolBarButton btnReload;
        private System.Windows.Forms.ToolBarButton btnOfflineSync;
    }
}