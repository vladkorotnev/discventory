﻿namespace Discventory
{
    partial class OfflineSyncForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс  следует удалить; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.lblEtc = new System.Windows.Forms.Label();
            this.lstResult = new System.Windows.Forms.ListBox();
            this.lblYear = new System.Windows.Forms.Label();
            this.lblAlbum = new System.Windows.Forms.Label();
            this.lblReleaseNumber = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbFolder = new System.Windows.Forms.ComboBox();
            this.lblBC = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItem2);
            // 
            // menuItem2
            // 
            this.menuItem2.Text = "Confirm && Next";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // lblEtc
            // 
            this.lblEtc.Location = new System.Drawing.Point(115, 120);
            this.lblEtc.Name = "lblEtc";
            this.lblEtc.Size = new System.Drawing.Size(119, 50);
            this.lblEtc.Text = "Russia CD Compilation Mixed";
            // 
            // lstResult
            // 
            this.lstResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstResult.Location = new System.Drawing.Point(4, 179);
            this.lstResult.Name = "lstResult";
            this.lstResult.Size = new System.Drawing.Size(230, 86);
            this.lstResult.TabIndex = 12;
            this.lstResult.Visible = false;
            this.lstResult.SelectedIndexChanged += new System.EventHandler(this.lstResult_SelectedIndexChanged_1);
            // 
            // lblYear
            // 
            this.lblYear.Location = new System.Drawing.Point(115, 98);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(122, 20);
            this.lblYear.Text = "2013";
            // 
            // lblAlbum
            // 
            this.lblAlbum.Location = new System.Drawing.Point(115, 51);
            this.lblAlbum.Name = "lblAlbum";
            this.lblAlbum.Size = new System.Drawing.Size(119, 47);
            this.lblAlbum.Text = "Armin van Buuren - A State Of Trance 2013";
            // 
            // lblReleaseNumber
            // 
            this.lblReleaseNumber.Location = new System.Drawing.Point(115, 31);
            this.lblReleaseNumber.Name = "lblReleaseNumber";
            this.lblReleaseNumber.Size = new System.Drawing.Size(122, 20);
            this.lblReleaseNumber.Text = "CD06438";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(4, 31);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(105, 106);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.Text = "Add to:";
            // 
            // cbFolder
            // 
            this.cbFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFolder.Location = new System.Drawing.Point(67, 3);
            this.cbFolder.Name = "cbFolder";
            this.cbFolder.Size = new System.Drawing.Size(170, 22);
            this.cbFolder.TabIndex = 11;
            // 
            // lblBC
            // 
            this.lblBC.Location = new System.Drawing.Point(4, 144);
            this.lblBC.Name = "lblBC";
            this.lblBC.Size = new System.Drawing.Size(100, 20);
            // 
            // OfflineSyncForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblBC);
            this.Controls.Add(this.lblEtc);
            this.Controls.Add(this.lstResult);
            this.Controls.Add(this.lblYear);
            this.Controls.Add(this.lblAlbum);
            this.Controls.Add(this.lblReleaseNumber);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbFolder);
            this.Menu = this.mainMenu1;
            this.Name = "OfflineSyncForm";
            this.Text = "Offline Sync";
            this.Load += new System.EventHandler(this.OfflineSyncForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.Label lblEtc;
        private System.Windows.Forms.ListBox lstResult;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Label lblAlbum;
        private System.Windows.Forms.Label lblReleaseNumber;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbFolder;
        private System.Windows.Forms.Label lblBC;
    }
}