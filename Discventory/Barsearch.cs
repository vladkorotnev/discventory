﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
#if DSIC_SCANNER
using DSIC.Barcode;
#endif

namespace Discventory
{
    public partial class Barsearch : Form
    {
        MainForm _callee = null;
        public Barsearch()
        {
            InitializeComponent();
            
        }
        public Barsearch(MainForm callee)
        {
            InitializeComponent();
            _callee = callee;
        }

        private void cbFolder_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        int scans = 0;
        List<DiscogsFolder> dirs;
        Toast myToast;
        private void Barsearch_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            myToast = new Toast(this, Color.White, "Added successfully");
            lstResult.Visible = false;
            lstResult.Items.Clear();
            pictureBox1.Image = null;
            lblAlbum.Text = "";
            lblReleaseNumber.Text = "";
            lblYear.Text = "";
            lblEtc.Text = "";

            dirs = DiscogsIntegration.GetFolders();
            foreach (DiscogsFolder dir in dirs)
            {
                cbFolder.Items.Add(dir.Name);
            }
            cbFolder.SelectedIndex = 0;
            Cursor.Current = Cursors.Default;

#if DSIC_SCANNER

            Wrapper.PDA_MODEL model = Wrapper.PDA_MODEL.UNKNOWN_MODEL;
            Wrapper.DSBarcodeGetPDAModel(ref model);
            if (model != Wrapper.PDA_MODEL.UNKNOWN_MODEL)
            {
                hasScanner = true;
                Wrapper.DSBarcodeOpen();

                Wrapper.DSBarcodeSetTimeOut(5000);

                Wrapper.DS3_DS5_SCANKEY keyOpt5 = new Wrapper.DS3_DS5_SCANKEY();
                {
                    keyOpt5.bLeft = true;
                    keyOpt5.bRight = true;
                    keyOpt5.bMiddle = true;
                    keyOpt5.bTrigger = true;
                }
                Wrapper.DSBarcodeSetDS3ScanKey(keyOpt5);

                Wrapper.DS7_SCANKEY keyOpt7 = new Wrapper.DS7_SCANKEY();
                {
                    keyOpt7.bLeftDown = true;
                    keyOpt7.bRightDown = true;
                }

                Wrapper.pCallBack barcodeCallback = new Wrapper.pCallBack(this.BarcodeEvent);
                Wrapper.DSBarcodeSetCallback(barcodeCallback);
                Wrapper.DSBarcodeSetSymbologyAllEnable();

                myToast.Text = "DSIC Scanner detected!";
                myToast.Show(2000);
            }
            else
            {
                
            }
#endif
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        UInt64 currentSelectionID = 0;

        IList<DiscogsAlbumSearchResult> searchResults;
        void ShowAlbum(DiscogsAlbumSearchResult album)
        {
            currentSelectionID = album.ID;
            if (album.Title != null) lblAlbum.Text = album.Title;
            string labels = "";
            if (album.CatNo != null)
            {
                labels += album.CatNo;
            }
            if (album.Label != null)
            {
                foreach (JToken lbl in album.Label)
                {
                    labels += lbl.Value<string>() + " ";
                }
            }

            lblReleaseNumber.Text = labels;
            if (album.Year != 0)
            {
                lblYear.Text = album.Year.ToString();
            }

            string formatStr = "";

            if (album.Country != null)
            {
                formatStr += album.Country;
            }
            if (album.Format != null)
            {
                foreach (JToken fmtTag in album.Format)
                {
                    formatStr += " " + fmtTag.Value<string>();
                }
            }
            lblEtc.Text = formatStr;

            if (album.ThumbnailURL != null && album.ThumbnailURL.Trim().Length > 0)
            {
                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy(); // fix for MobileBase DS-5
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(album.ThumbnailURL.Trim());
                request.UserAgent = "Discventory/1.0";

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    pictureBox1.Image = new Bitmap(stream);
                    request.Abort();
                }
            }
        }
        void GoSearch()
        {
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            lstResult.Visible = false;
            lstResult.Items.Clear();
            pictureBox1.Image = null;
            lblAlbum.Text = "";
            lblReleaseNumber.Text = "";
            lblYear.Text = "";
            lblEtc.Text = "";


            
            DiscogsSearchResults res = DiscogsIntegration.SearchDatabase(tbQuery.Text.Trim(), textBox1.Text.Trim());

            UInt64 itemCount = res.Items;
            searchResults = res.Content;

            if (itemCount == 0)
            {
                lblEtc.Text = "Not found";
                myToast.Text = "No match";
                myToast.Show(1000);
            }
            else if (itemCount == 1 || (firstMatch.Checked && autoAdd.Checked))
            {
                DiscogsAlbumSearchResult album = searchResults[0];
                ShowAlbum(album);

                if (autoAdd.Checked)
                {
                    addActive();
                }
            }
            else if (itemCount > 1)
            {
                
                foreach (DiscogsAlbumSearchResult album in searchResults)
                {
                    string line = "";
                    if (album.Year != 0) line += album.Year.ToString();
                    if (album.Country != null) line += " " + album.Country;
                    if (album.CatNo != null) line += " "+album.CatNo;
                    line += " " + album.Title;
                    lstResult.Items.Add(line);
                }
                lstResult.Visible = true;
            }
            if (!autoAdd.Checked)
            {
                
                    textBox1.Focus();
                    textBox1.SelectAll();
                if(hasScanner)
                {
                    reenableScanTimer.Enabled = true;
                }
            }
            Cursor.Current = Cursors.Default;
        }

        void addActive()
        {
          /*  if (currentSelectionID == -1)
            {
                MessageBox.Show("Nothing selected!", "Can't add", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            } */
            DiscogsFolder dir = dirs[cbFolder.SelectedIndex];
            
            try
            {
                DiscogsIntegration.AddToFolder(dir, currentSelectionID);
                if (!hasScanner)
                {
                    MessageBox.Show(String.Format("{0} added to {1}", lblAlbum.Text, (dir.ID == 0 ? "collection" : dir.Name)), "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    myToast.Text = "Successful add!";
                    System.Media.SystemSounds.Asterisk.Play();
                    myToast.Show(500);
                }
                if (_callee != null) _callee.InvalidateData();
             }
            catch (Exception e)
            {
                MessageBox.Show("Error occured:\n"+e.Message, "Can't add", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
          
                textBox1.Focus();
                textBox1.SelectAll();
            
            if(hasScanner)
            {
                reenableScanTimer.Enabled = true;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 32 || e.KeyChar == 13)
            {
                if (hasScanner)
                {
                    scans++;
                    this.Text = string.Format("{0} scans | Discventory", scans);
                }
                e.Handled = true;
                GoSearch();
                textBox1.SelectAll();
            }
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            GoSearch();
        }

        private bool hasScanner = false;
        private void Barsearch_Activated(object sender, EventArgs e)
        {
            textBox1.Focus();
            textBox1.SelectAll();
        }
#if DSIC_SCANNER
        private void BarcodeEvent(Wrapper.DS_BARCODE e)
        {
            textBox1.Text = e.strBarcode;
            GoSearch();
        }
#endif

        private void menuItem2_Click(object sender, EventArgs e)
        {
            addActive();
        }

        private void lstResult_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowAlbum(searchResults[lstResult.SelectedIndex]);
        }

        private void Barsearch_Closing(object sender, CancelEventArgs e)
        {
#if DSIC_SCANNER
            if (hasScanner)
            {
                reenableScanTimer.Enabled = false;
                Wrapper.DSBarcodeScanStop();
                Wrapper.DSBarcodeClose();
            }
#endif
        }

        private void textBox1_GotFocus(object sender, EventArgs e)
        {
            if (hasScanner)
            {
                reenableScanTimer.Enabled = true;
            }
        }

        private void reenableScanTimer_Tick(object sender, EventArgs e)
        {
            reenableScanTimer.Enabled = false;
#if DSIC_SCANNER
            Wrapper.DSBarcodeSetTimeOut(5000);
            Wrapper.DSBarcodeScanStart();
#endif
        }

       
    }
}