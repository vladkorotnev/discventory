﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Discventory
{
    class Toast
    {
        private Label _toastLabel;
        private Control _targetView;
        public Color BackgroundColor
        {
            get { return _toastLabel.BackColor; }
            set { _toastLabel.BackColor = value; }
        }
        public string Text
        {
            get { return _toastLabel.Text; }
            set { _toastLabel.Text = value; }
        }
        public Toast(Control inView, Color background, string Text)
        {
            _targetView = inView;
            _toastLabel = new Label();
            
            _toastLabel.Width = 2 * (_targetView.Width / 3);
            _toastLabel.Left = _targetView.Width / 6;

            _toastLabel.Height = _targetView.Height / 3;
            _toastLabel.Top = _targetView.Height / 3;

            _toastLabel.Font = new Font(FontFamily.GenericSansSerif, 16, FontStyle.Bold);
            _toastLabel.TextAlign = ContentAlignment.TopCenter;

            _toastLabel.Visible = false;
            _targetView.Controls.Add(_toastLabel);
        }
        private Timer t;
        public void Show(int ms)
        {
            _toastLabel.Visible = true;
            _toastLabel.BringToFront();
            if (t != null)
            {
                t.Enabled = false;
                t.Dispose();
                t = null;
            }
             t = new Timer();
            t.Interval = ms;
            t.Tick += new EventHandler(t_Tick);
            t.Enabled = true;
        }
        


        void t_Tick(object sender, EventArgs e)
        {
            _toastLabel.Visible = false;
            ((Timer)sender).Enabled = false;
            ((Timer)sender).Dispose();
            t = null;
           
        }

        static void ShowToast(Control inView, string Text, Color Background, int Time)
        {
            Toast t = new Toast(inView, Background, Text);
            t.Show(Time);
        }
    }
}
