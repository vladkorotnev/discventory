﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;

namespace Discventory
{
    /* Certificate Policy to allow all connections, because for some reason my DS5 doesn't like discogs.com certificate */
    public class TrustAllCertificatePolicy : System.Net.ICertificatePolicy
    {
        public TrustAllCertificatePolicy()
        { }

        public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
        {
            return true;
        }
    }

    /* Describes a folder item on Discogs */
    public struct DiscogsFolder
    {
        // Contains the folder name
        public string Name;
        // Contains the folder ID
        public UInt64 ID;
        // Contains the amount of items in folder
        public UInt64 Count;
    }

    // Primary class handling connections to Discogs API
    public static class DiscogsIntegration
    {
        // HTTP request method
        public  enum MethodType
        {
            // Perform a GET request
            GETMethod,
            // Perform a POST request
            POSTMethod
        }
        
        // Contains the current user name
        private static string _username;
        // Contains the current user ID
        private static Int64 _userid;

        // Returns the current logged in user name
        public static  string Username
        {
            get
            {
                return _username;
            }
        }

        // Returns the current logged in user ID
        public static  Int64 UserID
        {
            get
            {
                return _userid;
            }
        }

        // Contains the currently used personal API token
        public static string Token;

        // Converts a dictionary of parameters and their values into a URL-ready string
        public static string ArgArrayToUrlEncoded(Dictionary<string, string> args)
        {
            string result = "";
            foreach (string key in args.Keys)
            {
                result += string.Format("&{0}={1}", Uri.EscapeUriString(key), Uri.EscapeUriString(args[key]));
            }
            return result;
        }

        // Creates a URL with a token for the specified API endpoint and parameters
        public static string SignedURLStringForEndpointWithArguments(string endpoint, Dictionary<string, string> args)
        {
            string result = "https://api.discogs.com/";
            result += endpoint;
            result += "?token=" + Token;
            if (args != null)
            {
                result += "&"+ArgArrayToUrlEncoded(args);
            }
            return result;
        }

        // Sends a request to the specified URL using the specified method, and appending specified parameters, returns a decoded JSON object
        public static JObject QueryWithKnownURL(string URL, Dictionary<string, string> args, MethodType method)
        {
            System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy(); // fix for MobileBase DS-5
            HttpWebRequest rq = (HttpWebRequest)WebRequest.Create(URL);
            rq.KeepAlive = false;
            rq.UserAgent = "DiscVentory/1.0";
            rq.Method = (method == MethodType.GETMethod ? "GET" : "POST");
            if (method == MethodType.POSTMethod && args != null)
            {
                rq.ContentType = "application/x-www-form-urlencoded";
                byte[] postBytes;

                if (args != null)
                {
                    postBytes = Encoding.UTF8.GetBytes(ArgArrayToUrlEncoded(args));
                }
                else
                {
                    postBytes = new byte[0];
                }

                rq.ContentType = "application/x-www-form-urlencoded";
                rq.ContentLength = postBytes.Length;
                Stream rst = rq.GetRequestStream();
                rst.Write(postBytes, 0, postBytes.Length);
                rst.Close();
            }


            HttpWebResponse response;
            StreamReader reader;
            string responseString;
            response = (HttpWebResponse)rq.GetResponse();
            reader = new StreamReader(response.GetResponseStream());
            responseString = reader.ReadToEnd();
            JObject o = JsonConvert.DeserializeObject<JObject>(responseString);
            // If the API returned a message, display it
            if (o["message"] != null)
            {
                MessageBox.Show(((JToken)o["message"]).Value<string>(), "API Message", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
            rq.Abort();
            return o;
        }

        // Sends a query to the specified API endpoint of the specified method with provided parameters, returns a decoded JSON object
        public static JObject Query(string endpoint, Dictionary<string, string> args, MethodType method)
        {
            string url = SignedURLStringForEndpointWithArguments(endpoint, method == MethodType.GETMethod ? args : null);
            return QueryWithKnownURL(url, args, method);
         }

        // Verifies if the current user token is correct, returns true if token correct, otherwise false
        public static bool VerifyIdentity()
        {
            try
            {
                JObject res = Query("oauth/identity", null, MethodType.GETMethod);
                _userid = (Int64)res["id"];
                _username = (string)res["consumer_name"];
  
                return true;
            }
            catch 
            {
                return false;
            }
        }

        // Returns a list of current user's collection folders
        public static List<DiscogsFolder> GetFolders()
        {
            List<DiscogsFolder> dirs = new List<DiscogsFolder>();

            JArray q = (JArray)Query(String.Format("users/{0}/collection/folders", Username), null, MethodType.GETMethod)["folders"];
            foreach (JToken dd in q) {
                DiscogsFolder dir = new DiscogsFolder();
                dir.Name = dd["name"].Value<string>();
                dir.ID = dd["id"].Value<UInt64>();
                dir.Count = dd["count"].Value<UInt64>();
                dirs.Add(dir);
            }
            return  dirs;
        }

        // Returns the contents of the selected folder, starting from the specified page and using the requested sort order options
        public static DiscogsCollection GetFolderContents(DiscogsFolder folder, uint page, string sort, string sort_order)
        {
            Dictionary<string, string> args = new Dictionary<string, string>();
            args.Add("sort", sort == null ? "added":sort);
            args.Add("sort_order", sort_order == null ? "desc" : sort_order);
            args.Add("per_page", "50");
             if(page > 1) args.Add("page", page.ToString());
             return new DiscogsCollection(Query(string.Format("users/{0}/collection/folders/{1}/releases", Username, folder.ID), args, MethodType.GETMethod));
        }

        // Adds a release to the selected folder
        public static void AddToFolder(DiscogsFolder folder, UInt64 releaseID)
        {
            Query(string.Format("users/{0}/collection/folders/{1}/releases/{2}", Username, (folder.ID == 0? 1 : folder.ID), releaseID), null, MethodType.POSTMethod);
        }

        // Performs a search on the Discogs database by the specified query and barcode
        public static DiscogsSearchResults SearchDatabase(string query, string barcode)
        {
            Dictionary<string, string> args = new Dictionary<string, string>();

            args.Add("q", query);
            args.Add("barcode", barcode);

            JObject res = Query("database/search", args, DiscogsIntegration.MethodType.GETMethod);

            return new DiscogsSearchResults(res);
        }

        // Returns the current user's collection's root folder contents
        public static DiscogsCollection GetUserCollection(uint page, string sort, string sort_order)
        {
            DiscogsFolder root = new DiscogsFolder();
            root.ID = 0;
            return GetFolderContents(root, page, sort, sort_order);
        }
    }

    // General class for query results to encapsulate decoded JSON objects
    public class DiscogsQueryResult
    {
        protected JObject resultData;
        public DiscogsQueryResult(JObject source)
        {
            ReplaceResultData(source);
        }
        public DiscogsQueryResult() { }
        // Replaces the encapsulated JSON data with the specified object. Please note the object must be of the same type.
        public virtual void ReplaceResultData(JObject newData)
        {
            resultData = newData;
        }
    }

    // Contains a response from Discogs database spanned across multiple pages
    public class DiscogsPagedResult : DiscogsQueryResult 
    {
        public DiscogsPagedResult(JObject source) : base(source) { }
       
        // Returns the amount of items per one page
        public  UInt16 PerPage
        {
            get
            {
                if (resultData["pagination"] == null) return 0;
                return resultData["pagination"].Value<UInt16>("per_page");
            }
        }

        // Returns the content of the current page as a list of specified type objects
        public DiscogsList<T> ToList<T>() where T:DiscogsQueryResult,new() 
        {
            return new DiscogsList<T>(Content);
        }

        // Returns the total amount of items within query
        public  UInt64 Items
        {
            get
            {
                if (resultData["pagination"] == null) return 0;
                if (resultData["pagination"]["items"] == null) return 0;
                return resultData["pagination"].Value<UInt64>("items");
            }
        }

        // Returns the current page number
        public  UInt32 Page
        {
            get
            {
                if (resultData["pagination"] == null) return 0;
                if (resultData["pagination"]["page"] == null) return 0;
                return resultData["pagination"].Value<UInt32>("page");
            }
        }

        // Returns the total number of pages within query response
        public  UInt32 Pages
        {
            get
            {
                if (resultData["pagination"] == null) return 0;
                if (resultData["pagination"]["pages"] == null) return 0;
                return resultData["pagination"].Value<UInt32>("pages");
            }
        }

        // Returns whether the current page is the last page in the query response
        public bool IsLastPage
        {
            get
            {
                return Page == Pages;
            }
        }

        // Contains the key for the JSON object's property containing the query result
        protected string ContentKey = "results";

        // Returns the contents of the current page of the query
        public JArray Content
        {
            get
            {
                if (resultData[ContentKey] == null) return null;
                return (JArray)resultData[ContentKey];
            }
        }

        // Loads a pagination URL with the specified key, if that URL exists in the pagination data
        private void LoadPageKeyedAsIfExists(string paginationKey)
        {
            if (resultData["pagination"] != null)
            if (resultData["pagination"]["urls"] != null)
            if (resultData["pagination"]["urls"][paginationKey] != null)
            {
                JObject queryOutput = DiscogsIntegration.QueryWithKnownURL(resultData["pagination"]["urls"][paginationKey].Value<string>(), null, DiscogsIntegration.MethodType.GETMethod);
                this.ReplaceResultData(queryOutput);
            }
        }

        // Replaces the content with the contents of the query's next page, if such a page exists
        public void LoadNextPage()
        {
            LoadPageKeyedAsIfExists("next");
        }

        // Replaces the content with the contents of the query's previous page, if such a page exists
        public void LoadPrevPage()
        {
            LoadPageKeyedAsIfExists("prev");
        }

        // Replaces the content with the contents of the query's first page, if the query spans more than one page
        public void LoadFirstPage()
        {
            LoadPageKeyedAsIfExists("first");
        }

        // Replaces the content with the contents of the query's last page, if the query spans more than one page
        public void LoadLastPage()
        {
            LoadPageKeyedAsIfExists("last");
        }
    }

    // A class describing a resource on the Discogs database identifiable by a resource URL and ID
    public class DiscogsResource : DiscogsQueryResult 
    {
        public DiscogsResource() : base() { }
        public DiscogsResource (JObject source) : base(source) { }

        // Returns the API URL to request the current resource entity
        public string ResourceURL
        {
            get { return (resultData["resource_url"] == null) ? null:resultData["resource_url"].Value<String>(); }
        }

        // Returns the current resource entity's ID
        public UInt64 ID { get { return (resultData["id"] == null) ? 0 : resultData["id"].Value<UInt64>(); } }
    }

    // A class describing the community parameters of a release
    public class DiscogsCommunityData : DiscogsQueryResult
    {
        public DiscogsCommunityData(JObject source) : base(source) { }

        // Number of users that have the release
        public UInt32 Have
        {
            get { return (resultData["have"] == null) ? 0 : resultData["have"].Value<UInt32>(); }
        }

        // Number of users that want the release
        public UInt32 Want
        {
            get { return (resultData["want"] == null) ? 0 : resultData["want"].Value<UInt32>(); }
        }
    }

    // A class containing albums from a user's collection folder
    public class DiscogsCollection : DiscogsPagedResult
    {
        public DiscogsCollection(JObject source) : base(source) {
            ContentKey = "releases";
        }

        // Returns a list of the albums in the folder
        public DiscogsList<DiscogsCollectionAlbum> Albums
        {
            get
            {
                return this.ToList<DiscogsCollectionAlbum>();
            }
        }
    }

    // A class describing a named entity, such as a record label
    public class DiscogsNamedEntity : DiscogsResource
    {
        /* {
                "name": "RCA",
                "entity_type": "1",
                "catno": "PB 41447",
                "resource_url": "https://api.discogs.com/labels/895",
                "id": 895,
                "entity_type_name": "Label"
              } */
        public DiscogsNamedEntity() : base() { }
        public DiscogsNamedEntity(JObject source) : base(source) { }

        // Returns the name of the current entity, i.e. Pepita
        public string Name { get { return (resultData["name"] == null) ? null : resultData["name"].Value<string>(); } }

        // Returns the database entity type
        public Int32 EntityType { get { return (resultData["entity_type"] == null) ? 0 : resultData["entity_type"].Value<Int32>(); } }

        // Returns the entity's catalog number for the current release, if any, i.e. PB41447
        public string CatNo { get { return (resultData["catno"] == null) ? null:resultData["catno"].Value<string>(); } }

        // Returns the entity's user-readable name, i.e. Label
        public string EntityTypeName { get { return (resultData["entity_type_name"] == null) ? null:resultData["entity_type_name"].Value<string>(); } }
    }

    // A class describing a release's format (i.e. LP, CD, etc.)
    public class DiscogsCollectionFormat : DiscogsQueryResult
    {
        public DiscogsCollectionFormat() : base() { }
        public DiscogsCollectionFormat(JObject source) : base(source) { }
        /* {
                "descriptions": [
                  "7\"",
                  "45 RPM",
                  "Single"
                ],
                "name": "Vinyl",
                "qty": "1"
              } 
         */

        // Returns a list of descriptions for the format (i.e. 7", 45RPM, Single)
        public List<string> Descriptions
        {
            get
            {
                List<string> seq = new List<string>();
                if(resultData["descriptions"] != null)
                    foreach (JObject obj in resultData["descriptions"])
                    {
                        seq.Add(obj.Value<string>());
                    }
                return seq;
            }
        }

        // Returns the format name (i.e. Vinyl, CD, etc.)
        public string Name { get { return (resultData["name"] == null) ? null:resultData["name"].Value<string>(); } }

        // Returns the quantity of copies for the current format
        public UInt32 Quantity { get { return (resultData["qty"] == null) ? 0:resultData["qty"].Value<UInt32>(); } }
    }

    // A class that represents a list of DiscogsQueryResult or any of it's subclasses, automatically parsing the JSON objects from a provided array upon construction
    public class DiscogsList<T> : List<T> where T : DiscogsQueryResult, new()
    {
        
        public DiscogsList (JArray source) : base() {
            Fill(source);
        }

        // Fill the list with the contents of provided array, automatically converting the JSON objects to the type of list content
        public void Fill(JArray source)
        {
            this.Clear();
            foreach (JObject obj in source)
            {
                DiscogsQueryResult newObj = (DiscogsQueryResult) new T();
                newObj.ReplaceResultData(obj);
                this.Add((T)newObj);
            }
        }
    }

    // A class describing a release's artist or perfomrer
    public class DiscogsArtist : DiscogsResource
    {
        public DiscogsArtist() : base() { }
        
        public DiscogsArtist(JObject source) : base(source) { }
        /* {
                "join": ",",
                "name": "Rick Astley",
                "anv": "",
                "tracks": "",
                "role": "",
               - "resource_url": "https://api.discogs.com/artists/72872",
               - "id": 72872
              } */

        // Contains the text to use when joining the next artist's name to the current artist, i.e. "," or "feat."
        public string Join { get { return (resultData["join"] == null)?null:resultData["join"].Value<string>(); } }

        // Returns the name of the artist
        public string Name { get { return (resultData["name"] == null)?null:resultData["name"].Value<string>(); } }

        // Returns the name variation of the artist for the current release
        public string NameVariant { get { return (resultData["anv"] == null) ? "" : resultData["anv"].Value<string>(); } }

        // Returns the name variation for the current release, if any, otherwise normal artist name
        public string DisplayName { get { return ((NameVariant.Length == 0) ? Name : NameVariant); } }

        // Returns the text describing which tracks the artist participated on, if any
        public string Tracks { get { return (resultData["tracks"] == null) ? null : resultData["tracks"].Value<string>(); } }

        // Returns the role of the artist in the release, if any
        public string Role { get { return (resultData["role"] == null) ? null : resultData["role"].Value<string>(); } }
    }

    // A class representing a list of artists participating in the release
    public class DiscogsArtistList : DiscogsList<DiscogsArtist>
    {
        public DiscogsArtistList(JArray source)
            : base(source)
        {    
        }

        // Returns the user-readable text mentioning all artists on the release in order of appearance
        public override string ToString()
        {
            string result = "";
            for (int i = 0; i < this.Count; i++)
            {
                DiscogsArtist artist = this[i];
                if (result.Length > 0 && i>0) result += " "+this[i-1].Join+" ";
                result += artist.DisplayName;
            }
            return result;
        }
    }

    // A class representing a note for a release in the collection
    public class DiscogsNote : DiscogsQueryResult
    {
        public DiscogsNote(JObject source) : base(source) { }
        public DiscogsNote() : base() { }

        // Returns the note contents
        public string Value { get { return (resultData["value"] == null) ? null : resultData["value"].Value<string>(); } }

        // Returns the note's database field ID
        public UInt16 FieldID { get { return (resultData["field_id"] == null) ? (UInt16)0.0 : resultData["field_id"].Value<UInt16>(); } }
    }

    // A class representing an album in the user's collection
    public class DiscogsCollectionAlbum : DiscogsResource
    {
        public DiscogsCollectionAlbum() : base() { }
        public DiscogsCollectionAlbum(JObject source) : base(source) { _dateAdded = DateTime.Parse(resultData["date_added"].Value<string>()); }

        // Replaces the contained JSON object with the new provided data. Please note the object must comply to the same format.
        override public  void ReplaceResultData(JObject data)
        {
            base.ReplaceResultData(data);
            _dateAdded = DateTime.Parse(resultData["date_added"].Value<string>());
        }

        // Returns the instance-ID of the release within collection (not the release ID, because it is possible to have multiple variants of one release in collection)
        public UInt64 InstanceID { get { return (resultData["instance_id"] == null) ? (UInt64)0.0 : resultData["instance_id"].Value<UInt64>(); } }

        // Returns the user's rating of the release, 0~5
        public UInt16 Rating { get { return (resultData["rating"] == null) ? (UInt16)0.0 : resultData["rating"].Value<UInt16>(); } }

        // Returns a list of labels and other companies related to the release production
        public DiscogsList<DiscogsNamedEntity> Labels
        {
            get
            {
                if (resultData["basic_information"] == null) return null;
                if (resultData["basic_information"]["labels"] == null) return null;

                JArray labelArray = (JArray)resultData["basic_information"]["labels"];
                return new DiscogsList<DiscogsNamedEntity>(labelArray);
            }
        }

        // Returns the formats of the current release 
        public DiscogsList<DiscogsCollectionFormat> Formats
        {
            get
            {
                if (resultData["basic_information"] == null) return null;
                if (resultData["basic_information"]["formats"] == null) return null;

                JArray formatList = (JArray)resultData["basic_information"]["formats"];
                return new DiscogsList<DiscogsCollectionFormat>(formatList);
            }
        }

        private string _cachedUserReadableFormats = "";

        // Returns the formats of the current release as user-readable text ready for display (i.e. "2x LP")
        public string UserReadableFormats
        {
            get
            {
                if (_cachedUserReadableFormats.Length == 0)
                {
                    DiscogsList<DiscogsCollectionFormat> fmts = Formats;
                    if (fmts != null)
                    {
                        foreach (DiscogsCollectionFormat fmt in fmts)
                        {
                            if (_cachedUserReadableFormats.Length > 0) _cachedUserReadableFormats += ", ";
                            _cachedUserReadableFormats += string.Format("{0}x {1}", fmt.Quantity, fmt.Name);
                        }
                    }
                }
                return _cachedUserReadableFormats;
            }
        }

        // Returns the thumbnail URL for the album art, if any
        public string ThumbnailURL
        {
            get { return ((resultData["thumb"] == null) ? "" : resultData["basic_information"]["thumb"].Value<string>()); }
        }

        // Returns the release title
        public string Title { get {
            if (resultData["basic_information"] == null) return null;
            if (resultData["basic_information"]["title"] == null) return null;
            return resultData["basic_information"]["title"].Value<string>(); 
        } }

        // Returns a list of artists participating in the release
        public DiscogsArtistList Artists { get {
            if (resultData["basic_information"] == null) return null;
            if (resultData["basic_information"]["artists"] == null) return null;
            return new DiscogsArtistList((JArray)resultData["basic_information"]["artists"]);
        } }

        public string _userReadableArtistCache = "";
        
        // Returns the artists participating in the release as user-readable text ready for display
        public string UserReadableArtists
        {
            get
            {
                if (_userReadableArtistCache.Length == 0)
                {
                    DiscogsArtistList a = Artists;
                    if (a != null)
                        _userReadableArtistCache = a.ToString();
                }
                return _userReadableArtistCache;
            }
        }

        // Returns the API resource-URL for the current release
        public new string ResourceURL
        {
            
            get {
                if (resultData["basic_information"] == null) return null;
                if (resultData["basic_information"]["resource_url"] == null) return null; 
                return ((JObject)resultData["basic_information"])["resource_url"].Value<string>();
            }
        }
       
        // Returns the release's production year
        public UInt32 Year { get { 
                if (resultData["basic_information"]["year"] == null) return 0;
            return ((JObject)resultData["basic_information"])["year"].Value<UInt32>();
        } }

        // Returns the folder ID the release is contained in
        public UInt64 FolderID { get { 
            if (resultData["folder_id"] == null) return 0;
            return resultData["folder_id"].Value<UInt64>();
        } }

        private DateTime _dateAdded;
        
        // Returns the date and time the release was added to the collection
        public DateTime DateAdded { get {
           // if (resultData["date_added"] == null) return new DateTime(1970,1,1);
            return _dateAdded;
        } }

        // Returns a list of the user notes on the instance of the release in collection, including condition and custom fields
        public DiscogsList<DiscogsNote> Notes { get {
            if (resultData["notes"] == null) return null;
            return new DiscogsList<DiscogsNote>((JArray)resultData["notes"]); 
        } }

        private string _cachedUserReadableNotes = "";

        // Returns the user's notes as pre-rendered user-readable text ready for display
        public string UserReadableNotes
        {
            get
            {
                if (_cachedUserReadableNotes.Length == 0)
                {
                                DiscogsList<DiscogsNote> nlst = Notes;
                                if (nlst != null)
                                {
                                    foreach (DiscogsNote note in nlst)
                                    {
                                        if (_cachedUserReadableNotes.Length > 0) _cachedUserReadableNotes += "; ";
                                        _cachedUserReadableNotes += note.Value;
                                    }
                                }
                }

                return _cachedUserReadableNotes;
            }
        }

        private string _cachedUserReadableCatNo = "";
        
        // Returns the catalog number of the release pre-rendered as user-readable text ready for display
        public string UserReadableCatNo
        {
            get
            {
                if (_cachedUserReadableCatNo.Length == 0)
                {
                    DiscogsList<DiscogsNamedEntity> nlst = Labels;
                    if (nlst != null)
                    {
                        foreach (DiscogsNamedEntity label in nlst)
                        {
                            if (_cachedUserReadableCatNo.Length > 0) _cachedUserReadableCatNo += "; ";
                            _cachedUserReadableCatNo += /*label.Name + ": " +*/ label.CatNo;
                        }
                    }
                }
                
                
                return _cachedUserReadableCatNo;
            }
        }

        // In collection query:
      /*  {
          "instance_id": 222016095,
          "rating": 0,
          "basic_information": {
            "labels": [
              <DiscogsNamedEntity>
            ],
            "formats": [
              <DiscogsCollectionFormat>
            ],
            "thumb": "https://api-img.discogs.com/LBUVO680xZVHGHCK3ndEE-N1Nbs=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-249504-1334592212.jpeg.jpg",
            "title": "Never Gonna Give You Up",
            "artists": [
              <DiscogsArtist>
            ],
            O "resource_url": "https://api.discogs.com/releases/249504",
            "year": 1987,
            O "id": 249504
          },
          "folder_id": 1,
          "date_added": "2017-03-30T01:05:07-07:00",
          "notes": [
            {
              "field_id": 1,
              "value": "Good Plus (G+)"
            },
            {
              "field_id": 2,
              "value": "No Cover"
            },
            {
              "field_id": 3,
              "value": "Test"
            }
          ],
          "id": 249504
        }
         */
    }

    
    // A class that represents an album from the database search results
    public class DiscogsAlbumSearchResult : DiscogsResource
    {
        // In search
        /*
         * {
         *  "style": ["Prog Rock"],
         *  "thumb": "https://api-img.discogs.com/_-t5hZTE49nh7unyiRUE50r-RiI=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-1809801-1297364762.jpeg.jpg", 
         *  "format": [
         *      "Vinyl", "LP", "Album"
         *  ], 
         *  "country": "Hungary", 
         *  "barcode": [
         *      "ARTISJUS", 
         *      "SLPX 17579 - A  V1.  U. A.T.   2 - 17  ", 
         *      "SLPX  17579 - B V. P  MG"
         *   ], 
         *   "uri": "/Omega-Gammapolis/release/1809801", 
         *   "community": {
         *          "have": 132, 
         *          "want": 18
         *   }, 
         *   "label": [
         *      "Pepita"
         *    ],
         *    "catno": "SLPX 17579",
         *    "year": "1979", 
         *    "genre": ["Rock"], 
         *    "title": "Omega (5) - Gammapolis",
         *    "resource_url": "https://api.discogs.com/releases/1809801", 
         *    "type": "release", 
         *    "id": 1809801
         * }
         */

        public DiscogsAlbumSearchResult(JObject source) : base(source) { }
        public DiscogsAlbumSearchResult() : base() { }

        // Returns a JSON array of the album styles
        public JArray Style
        {
            get { return (JArray)resultData["style"]; }
        }

        // Returns a thumbnail url for the album art, if any
        public string ThumbnailURL
        {
            get { return (resultData["thumb"] == null) ? null : resultData["thumb"].Value<string>(); }
        }

        // Returns a JSON array of the album format description
        public JArray Format
        {
            get { return (JArray)resultData["format"]; }
        }

        // Returns the country the release was distributed in
        public string Country
        {
            get { return (resultData["country"] == null) ? null : resultData["country"].Value<string>(); }
        }

        // Returns a JSON array of the barcodes associated with the release
        public JArray Barcodes
        {
            get { return (JArray)resultData["barcode"]; }
        }

        // Returns a relative URL within the discogs.com website for the release
        public string RelativeURL
        {
            get { return (resultData["uri"] == null) ? null : resultData["uri"].Value<string>(); }
        }

        // Returns the community stats data for the release
        public DiscogsCommunityData Community
        {
            get { return (resultData["community"] == null) ? null : new DiscogsCommunityData((JObject)resultData["community"]); }
        }

        // Returns a JSON array of label names associated with the release
        public JArray Label
        {
            get { return (JArray)resultData["label"]; }
        }

        // Returns the catalog number of the release
        public string CatNo
        {
            get { return (resultData["catno"] == null) ? null : resultData["catno"].Value<string>(); }
        }

        // Returns the year the release was issued in
        public UInt32 Year
        {
            get { return (resultData["year"] == null) ? (UInt32)0 : resultData["year"].Value<UInt32>(); }
        }

        // Returns a JSON array of the genres associated with the release
        public JArray Genre
        {
            get { return (JArray)resultData["genre"]; }
        }

        // Returns the release title
        public string Title
        {
            get { return (resultData["title"] == null) ? null : resultData["title"].Value<string>(); }
        }

        // Adds the release to the specified folder
        public void AddToFolder(DiscogsFolder folder)
        {
            DiscogsIntegration.AddToFolder(folder, this.ID);
        }
    }

    // A class representing database search results
    public class DiscogsSearchResults : DiscogsPagedResult
    {
        public DiscogsSearchResults(JObject source)
            : base(source)
        {      
        }

        // Return the search result list
        public new DiscogsList<DiscogsAlbumSearchResult> Content
        {
            get
            {
                if (resultData["results"] == null) return null;
                return this.ToList<DiscogsAlbumSearchResult>();
            }
        }
    }
}
