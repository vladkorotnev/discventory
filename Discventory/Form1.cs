﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Discventory
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            this.Enabled = false;
            Application.DoEvents();

            DiscogsIntegration.Token = tokenBox.Text;
            if (DoLogin())
            {
                FileStream f = File.OpenWrite("discogs.tok");
                f.Seek(0, SeekOrigin.Begin);
                byte[] b = Encoding.UTF8.GetBytes(tokenBox.Text);
                int c = Encoding.UTF8.GetByteCount(tokenBox.Text);
                f.Write(b, 0, c);
                f.Close();
                LoggedIn();
            }
            else
            {
                MessageBox.Show("Something went wrong :(\nPlease check your token and try again.", "Ouch", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                this.Enabled = true;
                Cursor.Current = Cursors.Default;
            }
            
        }

        private bool DoLogin()
        {
            DiscogsIntegration.Token = tokenBox.Text;
            return DiscogsIntegration.VerifyIdentity();
        }

        private void LoggedIn()
        {
            MessageBox.Show(String.Format("Welcome back, {0}!", DiscogsIntegration.Username), "Success", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            MainForm f = new MainForm();
            f.ShowDialog();
            this.Close();
            Application.Exit();
        }


        bool didActivate;
        private void Form1_Activated(object sender, EventArgs e)
        {
            if (File.Exists("discogs.tok") && !didActivate)
            {
                didActivate = true;
                StreamReader ts = File.OpenText("discogs.tok");
                tokenBox.Text = ts.ReadToEnd();
                ts.Close();
               /* if (DoLogin())
                {
                    LoggedIn();
                }
                else
                {
                    MessageBox.Show("Something went wrong :(\nPlease check your token and try again.", "Ouch", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                } */
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            OfflineForm of = new OfflineForm();
            of.ShowDialog();
        }

       
    }
}