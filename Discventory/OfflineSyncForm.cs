﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;

namespace Discventory
{
    public partial class OfflineSyncForm : Form
    {
        MainForm _callee = null;
        public OfflineSyncForm()
        {
            InitializeComponent();
        }
        public OfflineSyncForm(MainForm callee)
        {
            InitializeComponent();
            _callee = callee;
        }
        List<String> offlineItems = new List<string>();
        UInt64 currentSelectionID = 0;
        List<DiscogsAlbumSearchResult> searchResults;
        List<DiscogsFolder> dirs;
        List<String> mismatch = new List<string>();

        void GoSearch()
        {
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();

            string q = offlineItems[0];
            lblBC.Text = q;
            searchResults = new List<DiscogsAlbumSearchResult>();
            searchResults.Clear();
            
            lstResult.Visible = false;
            lstResult.Items.Clear();
            pictureBox1.Image = null;
            lblAlbum.Text = "";
            lblReleaseNumber.Text = "";
            lblYear.Text = "";
            lblEtc.Text = "";



            DiscogsSearchResults res = DiscogsIntegration.SearchDatabase("", q);

            UInt64 itemCount = res.Items;
            foreach (DiscogsAlbumSearchResult sr in res.Content)
            {
                searchResults.Add(sr);
            }

            if (itemCount == 0)
            {
                // nothing found with that barcode, maybe it's Cat#?
                res = DiscogsIntegration.SearchDatabase(q, "");
                itemCount += res.Items;
                foreach (DiscogsAlbumSearchResult sr in res.Content)
                {
                    searchResults.Add(sr);
                }
            }

            if (itemCount == 0)
            {
                lblEtc.Text = "Not found";
                myToast.Text = "No match";
                mismatch.Add(q);
                myToast.Show(1000);
                showNext();
            }
            else if (itemCount == 1)
            {
                DiscogsAlbumSearchResult album = searchResults[0];
                ShowAlbum(album);
                addActive();
                showNext();
            }
            else if (itemCount > 1)
            {

                foreach (DiscogsAlbumSearchResult album in searchResults)
                {
                    string line = "";
                    if (album.Year != 0) line += album.Year.ToString();
                    if (album.Country != null) line += " " + album.Country;
                    if (album.CatNo != null) line += " " + album.CatNo;
                    line += " " + album.Title;
                    lstResult.Items.Add(line);
                }
                lstResult.Visible = true;
            }

            Cursor.Current = Cursors.Default;
        }

        void addActive()
        {
            /*  if (currentSelectionID == -1)
              {
                  MessageBox.Show("Nothing selected!", "Can't add", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
              } */
            DiscogsFolder dir = dirs[cbFolder.SelectedIndex];

            try
            {
                DiscogsIntegration.AddToFolder(dir, currentSelectionID);
                    myToast.Text = "Successful add!";
                    System.Media.SystemSounds.Asterisk.Play();
                    myToast.Show(500);
                
                if (_callee != null) _callee.InvalidateData();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error occured:\n" + e.Message, "Can't add", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }

     
        }

        void ShowAlbum(DiscogsAlbumSearchResult album)
        {
            currentSelectionID = album.ID;
            if (album.Title != null) lblAlbum.Text = album.Title;
            string labels = "";
            if (album.CatNo != null)
            {
                labels += album.CatNo;
            }
            if (album.Label != null)
            {
                foreach (JToken lbl in album.Label)
                {
                    labels += lbl.Value<string>() + " ";
                }
            }

            lblReleaseNumber.Text = labels;
            if (album.Year != 0)
            {
                lblYear.Text = album.Year.ToString();
            }

            string formatStr = "";

            if (album.Country != null)
            {
                formatStr += album.Country;
            }
            if (album.Format != null)
            {
                foreach (JToken fmtTag in album.Format)
                {
                    formatStr += " " + fmtTag.Value<string>();
                }
            }
            lblEtc.Text = formatStr;

            if (album.ThumbnailURL != null && album.ThumbnailURL.Trim().Length > 0)
            {
                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy(); // fix for MobileBase DS-5
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(album.ThumbnailURL.Trim());
                request.UserAgent = "Discventory/1.0";

                using (var response = request.GetResponse())
                using (var stream = response.GetResponseStream())
                {
                    pictureBox1.Image = new Bitmap(stream);
                    request.Abort();
                }
            }
        }

       
        private void menuItem2_Click(object sender, EventArgs e)
        {
            addActive();

            showNext();
           
        }

        private void showNext()
        {
          
            offlineItems.Remove(offlineItems[0]);
            this.Text = String.Format("{0} remaining", offlineItems.Count);

            if (offlineItems.Count == 0)
            {
                File.Delete("offline.dsk");
                if (mismatch.Count > 0)
                {
                    StreamWriter sw;
                    if (File.Exists("mismatch.txt"))
                    {
                        sw = new StreamWriter(File.OpenWrite("mismatch.txt"));
                        sw.BaseStream.Seek(0, SeekOrigin.End);
                    }
                    else
                    {
                        sw = File.CreateText("mismatch.txt");
                    }
                    foreach (string sas in mismatch)
                    {
                        sw.WriteLine(sas);
                    }
                    sw.Close();
                }
                MessageBox.Show("No more items! Mismatched entries, if any, were saved to mismatch.txt", "Yay", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                this.Close();
            }
            else
            {
                GoSearch();
            }
        }
        private void lstResult_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        Toast myToast;
        private void OfflineSyncForm_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            myToast = new Toast(this, Color.White, "Added successfully");
            lstResult.Visible = false;
            lstResult.Items.Clear();
            pictureBox1.Image = null;
            lblAlbum.Text = "";
            lblReleaseNumber.Text = "";
            lblYear.Text = "";
            lblEtc.Text = "";

            dirs = DiscogsIntegration.GetFolders();
            foreach (DiscogsFolder dir in dirs)
            {
                cbFolder.Items.Add(dir.Name);
            }
            cbFolder.SelectedIndex = 0;

            if (File.Exists("offline.dsk"))
            {
                StreamReader sr = File.OpenText("offline.dsk");
                while (!sr.EndOfStream)
                {
                    offlineItems.Add(sr.ReadLine());
                }
                sr.Close();
                GoSearch();
            }
            else
            {
                MessageBox.Show("No offline items!", "Yay", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                this.Close();
            }
            Cursor.Current = Cursors.Default;
        }

        private void lstResult_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            ShowAlbum(searchResults[lstResult.SelectedIndex]);
        }

        
    }
}