﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json.Linq;


namespace Discventory
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Closing(object sender, CancelEventArgs e)
        {
            DialogResult dr = MessageBox.Show("Do you want to log out?","Exit",MessageBoxButtons.YesNoCancel,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2);
            if (dr == DialogResult.Yes)
            {
                File.Delete("discogs.tok");
                Application.Exit();
            }
            else if (dr == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                Application.Exit();
            }
        }

        bool dataNeedsReloading = false;
        int curDirIndex = 0;
        List<DiscogsFolder> dirs;
        DiscogsCollection curDir;

        private void MainForm_Load(object sender, EventArgs e)
        {
            btnOfflineSync.Enabled = File.Exists("offline.dsk");
            this.Text = String.Format("{0}: Discventory", DiscogsIntegration.Username);
            Cursor.Current = Cursors.WaitCursor;
            dirs = DiscogsIntegration.GetFolders();
            foreach (DiscogsFolder dir in dirs)
            {
                    cbFolder.Items.Add(String.Format("{0} ({1})", dir.Name, dir.Count));
            }
            curDirIndex = cbFolder.SelectedIndex = 0;
            Cursor.Current = Cursors.Default;
        }
        public void InvalidateData()
        {
            dataNeedsReloading = true;
        }
        private void ReloadDir()
        {
            LoadDir(curDirIndex);
        }
        private void LoadDir(int DirIndex)
        {
            Cursor.Current = Cursors.WaitCursor;
            Application.DoEvents();
            curDirIndex = DirIndex;

            DiscogsFolder folderToLoad = dirs[curDirIndex];
            curDir = DiscogsIntegration.GetFolderContents(folderToLoad, 1, "added", "desc");
            UpdateDisplay();

            dataNeedsReloading = false;
            Cursor.Current = Cursors.Default;
        }

        private void UpdateDisplay()
        {
            btnFirst.Enabled = (curDir.Page > 1);
            btnPrev.Enabled = (curDir.Page > 1);
            btnNext.Enabled = (!curDir.IsLastPage);
            btnLast.Enabled = (!curDir.IsLastPage);
            this.Text = String.Format("[{1}/{2}] {3} - {0}: Discventory", DiscogsIntegration.Username, curDir.Page, curDir.Pages, dirs[curDirIndex].Name);
            CreateDataTable();
        }

        private void CreateDataTable()
        {
            table.TableStyles.Clear();
            table.DataSource = null;
            table.DataBindings.Clear();

            DataGridTableStyle dgts = new DataGridTableStyle();
            dgts.MappingName = curDir.Albums.GetType().Name;

            DataGridTextBoxColumn catNo = new DataGridTextBoxColumn();
            catNo.MappingName = "UserReadableCatNo";
            catNo.HeaderText = "Cat#";
            catNo.Width = 160;
            dgts.GridColumnStyles.Add(catNo);

            DataGridTextBoxColumn artist = new DataGridTextBoxColumn();
            artist.MappingName = "UserReadableArtists";
            artist.HeaderText = "Artist";
            artist.Width = 190;
            dgts.GridColumnStyles.Add(artist);

            DataGridTextBoxColumn title = new DataGridTextBoxColumn();
            title.MappingName = "Title";
            title.HeaderText = "Title";
            title.Width = 210;
            dgts.GridColumnStyles.Add(title);

            DataGridTextBoxColumn Year = new DataGridTextBoxColumn();
            Year.MappingName = "Year";
            Year.HeaderText = "Year";
            Year.Width = 60;
            dgts.GridColumnStyles.Add(Year);

            DataGridTextBoxColumn fmt = new DataGridTextBoxColumn();
            fmt.MappingName = "UserReadableFormats";
            fmt.HeaderText = "Fmt";
            fmt.Width = 110;
            dgts.GridColumnStyles.Add(fmt);

            DataGridTextBoxColumn note = new DataGridTextBoxColumn();
            note.MappingName = "UserReadableNotes";
            note.HeaderText = "Cond./Notes";
            note.Width = 150;
            dgts.GridColumnStyles.Add(note);

            DataGridTextBoxColumn rating = new DataGridTextBoxColumn();
            rating.MappingName = "Rating";
            rating.HeaderText = "Rating";
            rating.Width = 40;
            dgts.GridColumnStyles.Add(rating);

            DataGridTextBoxColumn added = new DataGridTextBoxColumn();
            added.MappingName = "DateAdded";
            added.HeaderText = "Date Added";
            added.Width = 210;
            dgts.GridColumnStyles.Add(added);

            table.DataSource = curDir.Albums;
            table.TableStyles.Add(dgts);
            
        }

        private void cbFolder_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDir(cbFolder.SelectedIndex);  
        }

        
        private void MainForm_Activated(object sender, EventArgs e)
        {
            if (dataNeedsReloading) ReloadDir();
        }

        private void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            if (e.Button == btnSearch)
            {
                Barsearch bs = new Barsearch(this);
                bs.ShowDialog();
            }
            else if (e.Button == btnOfflineSync)
            {
                OfflineSyncForm os = new OfflineSyncForm(this);
                os.ShowDialog();
            }
            else if (e.Button == btnLogout)
            {
                this.Close(); return;
            }
            else if (e.Button == btnReload)
            {

                this.ReloadDir();
            }
            else if (e.Button == btnFirst)
            {
                Cursor.Current = Cursors.WaitCursor;
                Application.DoEvents();
                curDir.LoadFirstPage();
                UpdateDisplay();
                Cursor.Current = Cursors.Default;
            }
            else if (e.Button == btnLast)
            {
                Cursor.Current = Cursors.WaitCursor;
                Application.DoEvents();
                curDir.LoadLastPage();
                UpdateDisplay();
                Cursor.Current = Cursors.Default;
            }
            else if (e.Button == btnPrev)
            {
                Cursor.Current = Cursors.WaitCursor;
                Application.DoEvents();
                curDir.LoadPrevPage();
                UpdateDisplay();
                Cursor.Current = Cursors.Default;
            }
            else if (e.Button == btnNext)
            {
                Cursor.Current = Cursors.WaitCursor;
                Application.DoEvents();
                curDir.LoadNextPage();
                UpdateDisplay();
                Cursor.Current = Cursors.Default;
            }
        }

     
    }
}