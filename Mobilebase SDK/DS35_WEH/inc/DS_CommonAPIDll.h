#if !defined(__DS_COMMONAPI_HEADER__)
#define __DS_COMMONAPI_HEADER__



#define DSAPI_EXC	extern "C"

#define DS_EXPORT
#ifdef DS_EXPORT
//dll export
#define DSAPI	__declspec(dllexport)
#else
//dll import
#define DSAPI	__declspec(dllimport)
#endif

#define MAX_LCD_LEVEL	6
#define MIN_LCD_LEVEL	1


DSAPI_EXC DSAPI BOOL DSAPISetVibrator	(int nTime);
DSAPI_EXC DSAPI BOOL DSAPISetRFPower	(BOOL bPwrOn);
DSAPI_EXC DSAPI BOOL DSAPIGetRFPower	(BOOL *bPwrState);
DSAPI_EXC DSAPI BOOL DSAPISetCameraFlashOnOff(BOOL bOn);

//20131008 jinni.
DSAPI_EXC DSAPI BOOL DSAPIGetWLANPower	(BOOL *bPwrState);
DSAPI_EXC DSAPI BOOL DSAPISetWLANPower	(BOOL bPwrOn);		

//20140319 jinni. [DS5] only
DSAPI_EXC DSAPI BOOL DSAPISetFrontLEDOnOff(BOOL bPwrOn);

#endif // !defined(__DS_COMMONAPI_HEADER__)