#if !defined(_MB_BarcodeWrapper_H_)
#define _MB_BarcodeWrapper_H_


#define MBAPI_EXC	extern "C"
//#define MB_EXPORT
#ifdef MB_EXPORT
//dll import
#define MBAPI	__declspec(dllimport)
#else
//dll export
#define MBAPI	__declspec(dllexport)
#endif


typedef enum _OS_VERSION
{
	UNKNOWN_VERSION,
	CE_5,
	CE_6,
	WM_65,
}OS_VERSION;

typedef enum _PDA_MODEL
{
	UNKNOWN_MODEL,
	DS3,
	DS31,
	DS7,
	DS5,
}PDA_MODEL;

//SPEC_EVENT ���� MESSAGE
#define	WM_MBSCANNER_READBARCODE			WM_USER + 100
#define	WM_MBSCANNER_READBARCODEFAIL		WM_USER + 101

//error code
typedef enum _eMB_RESULT
{
	//ERROR CODE
	MBRES_SUCCESS				= 1,
	MBRES_ERR					= -1,
	MBRES_ERR_PARAMETER			= -2,			//MBImager -2~-100
	MBRES_ERR_NOT_PREVIEWING	= -3,
	MBRES_ERR_NOT_PREVIEWPAUSE	= -4,
	MBRES_ERR_PREVIEWING		= -5,
	MBRES_ERR_OPENED			= -6,
	MBRES_ERR_NOTOPENED			= -7,
	MBRES_ERR_OEMOPENFAIL		= -8,
	MBRES_ERR_OEMCLOSEFAIL		= -9,
	MBRES_ERR_USING_CAMERA		= -10,
	MBRES_ERR_CAMERARESPONSEFAIL	 = -11,
	MBRES_ERR_LACK_MEMORY = -12,
	MBRES_ERR_NOT_SUPPORT = -13,

	MBRES_ERR_DRIVER_NOT_LOAD	= -50,//SE4500
	MBRES_ERR_IOCTL				= -51,
	MBRES_ERR_ATTACH_SCANNER	= -52,
	MBRES_ERR_ON_DECORDING		= -53,
	MBRES_ERR_ON_CAPTURE		= -54,
}MB_RESULT;

typedef enum _eSCAN_RESULT
{
	SCAN_FAIL = 0,
	SCAN_SUCCESS = 1
}SCAN_RESULT;

typedef enum _eSND_TYPE
{
	MUTE = 0,
	SND = 1,
	VIB = 2,
	SNDANDVIB = 3,//ADDED TYPE(SOUND AND VIB)
}SND_TYPE;


typedef struct _DS3_DS5_SCANKEY
{
	bool bLeft;
	bool bRight;
	bool bMiddle;
	bool bTrigger;
}DS3_DS5_SCANKEY;

typedef struct _DS7_SCANKEY
{
	bool bRightUp;
	bool bRightDown;
	bool bLeftUp;
	bool bLeftDown;
	bool bNaviUp;
	bool bNaviDown;
	bool bNaviLeft;
	bool bNaviRight;
}DS7_SCANKEY;

typedef struct tagDS_BARCODE
{
	LPTSTR			lptBarcode;
}DS_BARCODE, *PDS_BARCODE;

typedef void (*PFNCALLBACK)(DS_BARCODE DecodingData);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeOpen();
MBAPI_EXC MBAPI MB_RESULT DSBarcodeClose();
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetWindow(HWND hWnd);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetCallback(PFNCALLBACK pfnCALLBACK);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeScanStart();
MBAPI_EXC MBAPI MB_RESULT DSBarcodeScanStop();

//Timeout
//Default -> 3000(3Sec)
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetTimeOut(int nTimeOut);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetTimeOut(int* pnTimeOut);

//Notification
//Default -> SCAN SUCCESS : 
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetSoundType(SCAN_RESULT bResult, SND_TYPE bType);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetSoundType(SCAN_RESULT bResult, SND_TYPE* pbType);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetSoundPath(SCAN_RESULT bResult, TCHAR bPath[512]);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetSoundPath(SCAN_RESULT bResult, TCHAR* pbPath);

//DS3/DS5 KEY
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetDS3ScanKey(DS3_DS5_SCANKEY stDS3Key);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetDS3ScanKey(DS3_DS5_SCANKEY *pstDS3Key);

//DS7 KEY
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetDS7ScanKey(DS7_SCANKEY stDS7Key);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetDS7ScanKey(DS7_SCANKEY *pstDS7Key);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetEnable(bool bEnable);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetBarcodeSetting(TCHAR *ptzDestPath = NULL);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetBarcodeSetting(TCHAR *ptzLocalPath = NULL);


#endif