#if !defined(__DS_COMMONAPI_HEADER__)
#define __DS_COMMONAPI_HEADER__



#define DSAPI_EXC	extern "C"
#define DSAPI		__declspec(dllexport)

#define MAX_LCD_LEVEL	6
#define MIN_LCD_LEVEL	1

typedef enum _eMODELTYPE
{
	DS_TYPE_ERROR = -1,
	DS_3 = 1,
	DS_7,
	DS_31,
	DS_5,
}DS_MODELTYPE;

typedef enum _eREADER_TYPE
{
	UNKNOWN = -1,
	SCANNER = 0,
	IMAGER,
	IMAGER_GEN6,
	IMAGER_SE4500,
	SCANNER_N4313,
	IMAGER_SE4750,
	MAX_READER,
}DS_READER_TYPE;

//[WM] only
typedef enum _eKEY_MODE
{
	VKEY_MODE = 1,					//DS3 & DS3.1 & DS5 34Key & DS5 54Key
	FKEY_MODE = 2,					//DS5 34Key & DS5 54Key
	AKEY_MODE = 3,					//DS5 54Key only
}DS_KEY_MODE;

//[WM] only
typedef struct _stKEY_MAP
{
	DS_KEY_MODE 	KeyMode;
	BOOL			bShift;			//VKEY_MODE or AKEY_MODE
	byte			yKeyCode;
}DS_KEY_MAP, *PDS_KEY_MAP;


DSAPI_EXC DSAPI BOOL DSAPISetVibrator	(int nTime);
DSAPI_EXC DSAPI BOOL DSAPISetRFPower	(BOOL bPwrOn);
DSAPI_EXC DSAPI BOOL DSAPIGetRFPower	(BOOL *bPwrState);
DSAPI_EXC DSAPI BOOL DSAPISetCameraFlashOnOff(BOOL bOn);

//20131008 jinni.
DSAPI_EXC DSAPI BOOL DSAPIGetWLANPower	(BOOL *pbPwrState);
DSAPI_EXC DSAPI BOOL DSAPISetWLANPower	(BOOL bPwrOn);		

//20140319 jinni. [DS5] only
DSAPI_EXC DSAPI BOOL DSAPISetFrontLEDOnOff(BOOL bPwrOn);

DSAPI_EXC DSAPI DS_MODELTYPE DSAPIGetModel();
DSAPI_EXC DSAPI DS_READER_TYPE DSAPIGetBarcodeType();

//20141010 jinni. [WM] only (v 2.0.0.8)
DSAPI_EXC DSAPI BOOL DSAPIKeyMapping_RegisterKey(UINT nVKey, UINT nVKeyNew);
DSAPI_EXC DSAPI BOOL DSAPIKeyMapping_UnregisterKey(UINT nVKey);

//20150306 jinni. [WM] only (v 2.0.0.11, DS3, DS3.1, DS5)
DSAPI_EXC DSAPI BOOL DSAPIKeyMapping_RegisterKeyA(DS_KEY_MODE enKeyMode, UINT nIndex, BYTE btKeyCode);
DSAPI_EXC DSAPI BOOL DSAPIKeyMapping_UnregisterKeyA(DS_KEY_MODE enKeyMode, UINT nIndex);

//20141125 jinni. [WM] only 
//BarcodeTray scan Key Register/Unregister function. (v 2.0.0.9)
DSAPI_EXC DSAPI BOOL DSAPIBarcodeTraySCANKeyEnable(UINT nVKey, BOOL bEnable);

//20150326 jinni.
DSAPI_EXC DSAPI BOOL DSAPIKeyMapping_Flush(DS_KEY_MODE enKeyMode);
DSAPI_EXC DSAPI BOOL DSAPIKeyMapping_Query(DS_KEY_MODE enKeyMode);

//20150407 jinni.
DSAPI_EXC DSAPI void DSAPIReboot();

//20150703 jinni. [WCE6] only  (v 2.0.0.17)
DSAPI_EXC DSAPI BOOL DSAPIGetBTPower	(BOOL *bPwrState);
DSAPI_EXC DSAPI BOOL DSAPISetBTPower	(BOOL bPwrOn);		



#endif // !defined(__DS_COMMONAPI_HEADER__)