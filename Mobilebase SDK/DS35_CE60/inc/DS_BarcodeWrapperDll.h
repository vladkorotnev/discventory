#if !defined(_MB_BarcodeWrapper_H_)
#define _MB_BarcodeWrapper_H_


#define MBAPI_EXC	extern "C"
//#define MB_EXPORT
#ifdef MB_EXPORT
//dll import
#define MBAPI	__declspec(dllimport)
#else
//dll export
#define MBAPI	__declspec(dllexport)
#endif


typedef enum _OS_VERSION
{
	UNKNOWN_VERSION,
	CE_5,
	CE_6,
	WM_65,
}OS_VERSION;

typedef enum _PDA_MODEL
{
	UNKNOWN_MODEL,
	DS3,
	DS31,
	DS7,
	DS5,
}PDA_MODEL;


typedef enum _BARCODE_READER_TYPE
{
	UNKNOWN_READER_TYPE = -1,
	SE955 = 0,
	IT5300,
	N5600,
	SE4500,
	N4313,
	SE4750,
}BARCODE_READER_TYPE;

//SPEC_EVENT ���� MESSAGE
#define	WM_MBSCANNER_READBARCODE			WM_USER + 100
#define	WM_MBSCANNER_READBARCODEFAIL		WM_USER + 101

//error code
typedef enum _eMB_RESULT
{
	//ERROR CODE
	MBRES_SUCCESS				= 1,
	MBRES_ERR					= -1,
	MBRES_ERR_PARAMETER			= -2,			//MBImager -2~-100
	MBRES_ERR_NOT_PREVIEWING	= -3,
	MBRES_ERR_NOT_PREVIEWPAUSE	= -4,
	MBRES_ERR_PREVIEWING		= -5,
	MBRES_ERR_OPENED			= -6,
	MBRES_ERR_NOTOPENED			= -7,
	MBRES_ERR_OEMOPENFAIL		= -8,
	MBRES_ERR_OEMCLOSEFAIL		= -9,
	MBRES_ERR_USING_CAMERA		= -10,
	MBRES_ERR_CAMERARESPONSEFAIL	 = -11,
	MBRES_ERR_LACK_MEMORY = -12,
	MBRES_ERR_NOT_SUPPORT = -13,

	MBRES_ERR_DRIVER_NOT_LOAD	= -50,//SE4500
	MBRES_ERR_IOCTL				= -51,
	MBRES_ERR_ATTACH_SCANNER	= -52,
	MBRES_ERR_ON_DECORDING		= -53,
	MBRES_ERR_ON_CAPTURE		= -54,
	MBRES_ERR_NOT_SUPPROT_SYMBOLOGY	= -55,
}MB_RESULT;

typedef enum _eSCAN_RESULT
{
	SCAN_FAIL = 0,
	SCAN_SUCCESS = 1
}SCAN_RESULT;

typedef enum _eSND_TYPE
{
	MUTE = 0,
	SND = 1,
	VIB = 2,
	SNDANDVIB = 3,//ADDED TYPE(SOUND AND VIB)
}SND_TYPE;

//1.0.0.3 ADDED(+)
#define KEY_OPTION_NONE		0x00000000
#define KEY_OPTION_CODEPAGE 0x00000001
typedef struct _KEY_OPTION
{
	int nCodePage;
	int nKeyOption;
}KEY_OPTION;

typedef struct _DS3_DS5_KEY_OPTION
{
	KEY_OPTION enLeft;
	KEY_OPTION enRight;
	KEY_OPTION enMiddle;
	KEY_OPTION enTrigger;
}DS3_DS5_KEY_OPTION;


typedef struct _DS7_KEY_OPTION
{
	KEY_OPTION enRightUp;
	KEY_OPTION enRightDown;
	KEY_OPTION enLeftUp;
	KEY_OPTION enLeftDown;
	KEY_OPTION enNaviUp;
	KEY_OPTION enNaviDown;
	KEY_OPTION enNaviLeft;
	KEY_OPTION enNaviRight;
}DS7_KEY_OPTION;


//1.0.0.3 ADDED(-)



typedef struct _DS3_DS5_SCANKEY
{
	bool bLeft;
	bool bRight;
	bool bMiddle;
	bool bTrigger;
}DS3_DS5_SCANKEY;

typedef struct _DS7_SCANKEY
{
	bool bRightUp;
	bool bRightDown;
	bool bLeftUp;
	bool bLeftDown;
	bool bNaviUp;
	bool bNaviDown;
	bool bNaviLeft;
	bool bNaviRight;
}DS7_SCANKEY;

typedef enum _SYBOLOGY
{
	UNKNOWN_TYPE = -1,//1.0.0.7 Added
	AustraliaPost = 0,
	Aztec,
	BooklandEAN,
	BPO,
	CanadaPost,
	ChinaPost,
	Codabar,
	Codablock,
	Code11,
	Code128,
	Code128Emultion,
	Code16k,
	Code32,
	Code32_PARAF,
	Code39,
	Code49,
	Code93,
	Composite_CCAB,
	Composite_CCC,
	Composite_TLC39,
	CompositeCode,
	CouponCode,
	DataMatrix,
	Discrete2of5,
	DutchPost,
	EAN128,
	EAN13,
	EAN13_2Supps,
	EAN13_5Supps,
	EAN8,
	GS1_128,
	GS1_Databar14,
	GS1_DATABAR_Expanded,
	GS1_DATABAR_Limited,
	GS1_DATABAR_OMNIDIRECTIONAL,
	HANXIN,
	IDTag,
	Interleaved2of5,
	ISBT128,
	JapanPost,
	KoreanPost,
	Matrix2of5,
	MaxiCode,
	MESA,
	MicroPDF,
	MicroQR,
	MSI,
	OCR,
	NetherlandKIX,
	Mx25,
	NEC2OF5,
	PDF417,
	Planet,
	Plessey,
	PosiCode,
	Postals,
	Postnet,
	QR,
	RSS,
	RSS14,
	RSSExpanded,
	RSSLimited,
	Straight2of5,
	Straight2of5_IATA,
	Straight2of5_Industrial,
	Telepen,
	TriopticCode39,
	TLC39,
	UKPostal,
	UPCA,
	UPCA_2Supps,
	UPCA_5Supps,
	UPCE0,
	UPCE0_2Supps,
	UPCE0_5Supps,
	UPCE1,
	UPCE1_2Supps,
	UPCE1_5Supps,
	UPUFICS,
	US_Postal1,
	US_Postnet,
	USPlanet,
	USPSS4CB,
}SYMBOLOGY;


typedef struct tagDS_BARCODE
{
	LPTSTR			lptBarcode;
}DS_BARCODE, *PDS_BARCODE;


typedef struct tagDS_BARCODE_WITH_TYPE
{
	SYMBOLOGY		enSymbologyType;
	LPTSTR			lptBarcode;
}DS_BARCODE_WITH_TYPE, *PDS_BARCODE_WITH_TYPE;


typedef void (*PFNCALLBACK)(DS_BARCODE DecodingData);
typedef void (*PFNCALLBACK_WITH_TYPE)(DS_BARCODE_WITH_TYPE DecodingData);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeOpen();
MBAPI_EXC MBAPI MB_RESULT DSBarcodeClose();
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetWindow(HWND hWnd);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetCallback(PFNCALLBACK pfnCALLBACK);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetCallbackWithType(PFNCALLBACK_WITH_TYPE pfnCALLBACK);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeScanStart();
MBAPI_EXC MBAPI MB_RESULT DSBarcodeScanStop();

//Timeout
//Default -> 3000(3Sec)
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetTimeOut(int nTimeOut);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetTimeOut(int* pnTimeOut);

//Notification
//Default -> SCAN SUCCESS : 
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetSoundType(SCAN_RESULT bResult, SND_TYPE bType);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetSoundType(SCAN_RESULT bResult, SND_TYPE* pbType);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetSoundPath(SCAN_RESULT bResult, TCHAR bPath[512]);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetSoundPath(SCAN_RESULT bResult, TCHAR* pbPath);

//DS3/DS5 KEY
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetDS3ScanKey(DS3_DS5_SCANKEY stDS3Key);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetDS3ScanKey(DS3_DS5_SCANKEY *pstDS3Key);

//DS7 KEY
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetDS7ScanKey(DS7_SCANKEY stDS7Key);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetDS7ScanKey(DS7_SCANKEY *pstDS7Key);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetEnable(bool bEnable);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetBarcodeSetting(TCHAR *ptzDestPath = NULL);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetBarcodeSetting(TCHAR *ptzLocalPath = NULL);

//1.0.0.3 ADDED(+)
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetDS3Keyoptions(DS3_DS5_KEY_OPTION *pstOption);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetDS3Keyoptions(DS3_DS5_KEY_OPTION stOption);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetDS7Keyoptions(DS7_KEY_OPTION *pstOption);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetDS7Keyoptions(DS7_KEY_OPTION stOption);
//1.0.0.3 ADDED(-)

//1.0.0.6 ADDED(+)
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetSymbologyAllEnable();
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetSymbologyAllDisable();
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetSymbologyEnable(SYMBOLOGY enSymbology,bool *pbEnable);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeSetSymbologyEnable(SYMBOLOGY enSymbology,bool bEnable);

MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetOSVersion(OS_VERSION *enOSVersion);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetPDAModel(PDA_MODEL *enPDAModel);
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetBarcodeModuleType(BARCODE_READER_TYPE *enBarcodeReaderType);
//1.0.0.6 ADDED(-)

//1.0.0.7 ADDED(+)
MBAPI_EXC MBAPI MB_RESULT DSBarcodeGetSymbologyFromMsgWParam(WPARAM wParam,SYMBOLOGY *penBarcodeType);
//1.0.0.7 ADDED(-)

//1.0.0.8 ADDED(+)

//1.0.0.8 ADDED(-)


#endif